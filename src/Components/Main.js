import React, {Component} from 'react'
import SubcomponentA from './SubcomponentA'
import axios from 'axios'

const URL = 'https://jsonplaceholder.typicode.com/users'
const URL2 = 'https://jsonplaceholder.typicode.com/posts'

class Main extends Component{
  state = {
    title: 'Appointments',
    show: true,
    data: [],
    posts: []
  }

  componentDidMount(){
    axios.get(URL)
      .then(response =>  {
        const data = response.data
        this.setState({data})
      })
    
    //este aquiserá usado pelo subcomponente
    axios.get(URL2)
      .then(response => {
        const posts = response.data 
        this.setState({posts})
      })
  }

  deletePost = (item) => {
    const postUpdated = this.state.posts.filter(p => p !== item)
    this.setState({
      posts: [...postUpdated]
    })
  }

  render(){
    /**Colocamos dados complexos fora do return().
     * Neste exemplo, as duas variáveis abaixo serão exibidas dependendo da 
     * condição do estado.
     */
    let showTitle
    if(this.state.show){
      showTitle = 'New'
    }

    let displayList = {
      display: this.state.show ? 'block' : 'none',
      color: 'red'
    }

    /**Mapeando dados complexos */
    let filterUsers = this.state.data
    filterUsers = filterUsers.map((item, index) => {
      //console.log(item.name)
      return(
        
        <ul key={index}>
          <li>Name: {item.name}</li>
          <li>Username: {item.username}</li>
          <li>Email: {item.email}</li>
        </ul>
      )
    })

    //Iterando o subcomponentA
    let listPosts = this.state.posts
    listPosts = listPosts.map((item, index) => {
      return(
        <SubcomponentA 
          key={item.id} //chamamos key dentro do loop, não foi definido no subcomponente
          numPost={item.id}
          title={item.title}
          body={item.body}
          whichItem = {item}
          onDelete={this.deletePost} //chama método definido aqui
        />
      )
    })

    return(
      <div>
        <h1>{showTitle} {this.state.title}</h1>
        <ul style={displayList}>
          <li>Buffy 3:30 PM</li>
          <li>Spot 8:30 PM</li>
          <li>Goldie 3:50 PM</li>
        </ul>
        {filterUsers}

        <h2>Subcomponent A:</h2>
        {listPosts}
      </div>
    )
  }
}

export default Main