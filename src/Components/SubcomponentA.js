/**
 * O componente principal (Main.js) não pode passar estados para os subcomponentes.
 * Para passar informações, usamos props
 */

import React from 'react'

class SubcomponentA extends React.Component{

  handleDelete = () => {
    //temos duas novas props aqui: onDelete e wichItem
    this.props.onDelete(this.props.whichItem)
  }

  render(){
    const {title, body, numPost} = this.props
    return(
      <div>
        <ul>
          <li>Nº {numPost}</li>
          <li>Title: {title}</li>
          <li>Body: {body}</li>
        </ul>

        {/*Exemplo de event: deletando post */}
        <button onClick={this.handleDelete}>Delete post {numPost}</button>
      </div>
    )
  }
 }

 export default SubcomponentA